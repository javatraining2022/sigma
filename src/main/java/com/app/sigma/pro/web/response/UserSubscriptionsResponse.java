package com.app.sigma.pro.web.response;

import java.util.List;

import com.app.sigma.pro.data.jpa.persistance.UserSubscription;
public class UserSubscriptionsResponse extends BaseResponse {
	private List<UserSubscription> userSubscriptions;

	public List<UserSubscription> getUserSubscriptions() {
		return userSubscriptions;
	}

	public void setUserSubscriptions(List<UserSubscription> userSubscriptions) {
		this.userSubscriptions = userSubscriptions;
	}
	
}
