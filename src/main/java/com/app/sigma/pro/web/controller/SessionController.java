package com.app.sigma.pro.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.app.sigma.pro.data.request.CreateCourseVideoRequest;
import com.app.sigma.pro.data.request.SessionRequest;
import com.app.sigma.pro.data.service.CourseVideoService;
import com.app.sigma.pro.data.service.SessionService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.Authorization;

@Api(value = SessionController.API_CATEGORY, tags = { SessionController.API_TAGS })
@RestController
@RequestMapping("/api/sigma")
@CrossOrigin
public class SessionController {
	
	@Autowired
	private SessionService sessionService;
	@Autowired
	private CourseVideoService courseVideoService;
	
	/** Category to which these APIs belong. */
	public static final String API_CATEGORY = "Sigma Pro Session";

	/** Tags to be associated to identify these APIs. */
	public static final String API_TAGS = "Sigma Pro Session";

	@ApiOperation(value = "This API is used to Start the Session.", nickname = "startSession", tags = {
			SessionController.API_TAGS }, authorizations = { @Authorization(value = "Bearer") })
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Session Requested SuccessFully.") })
	@PostMapping(value="/session/start")
	public ResponseEntity<?> startSession(@Validated @RequestBody SessionRequest session) throws Exception {
		return ResponseEntity.ok(sessionService.startSession(session));
	}
	
	@ApiOperation(value = "This API is used to get the Session's for the Course.", nickname = "getSessions", tags = {
			SessionController.API_TAGS }, authorizations = { @Authorization(value = "Bearer") })
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Session's fetched SuccessFully.") })
	@GetMapping(value="/sessions")
	public ResponseEntity<?> getSessions(@RequestParam long courseId) throws Exception {
		return ResponseEntity.ok(sessionService.fetchSessions(courseId));
	}
	
	@ApiOperation(value = "This API is used to Create CourseVideo the Session.", nickname = "createCourseVideo", tags = {
			SessionController.API_TAGS }, authorizations = { @Authorization(value = "Bearer") })
	@ApiResponses(value = { @ApiResponse(code = 200, message = "CourseVideo Created SuccessFully.") })
	@PostMapping(value="/coursevideo/create")
	public ResponseEntity<?> createCourseVideo(@Validated @RequestBody CreateCourseVideoRequest courseVideo) throws Exception {
		return ResponseEntity.ok(courseVideoService.createVideo(courseVideo));
	}
	
	@ApiOperation(value = "This API is used to get the CourseVideos for the Course.", nickname = "getCourseVideos", tags = {
			SessionController.API_TAGS }, authorizations = { @Authorization(value = "Bearer") })
	@ApiResponses(value = { @ApiResponse(code = 200, message = "CourseVideos fetched SuccessFully.") })
	@GetMapping(value="/coursevideos")
	public ResponseEntity<?> getCourseVideos(@RequestParam long courseId) throws Exception {
		return ResponseEntity.ok(courseVideoService.fetchCourseVideos(courseId));
	}
	
	
}
