package com.app.sigma.pro.web.response;

import com.app.sigma.pro.data.jpa.persistance.User;


public class UserResponse extends BaseResponse {
private User user;

public User getUser() {
	return user;
}

public void setUser(User user) {
	this.user = user;
}


}
