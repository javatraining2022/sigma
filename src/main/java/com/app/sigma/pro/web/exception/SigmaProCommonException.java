package com.app.sigma.pro.web.exception;

public class SigmaProCommonException extends RuntimeException{
 /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

public SigmaProCommonException(String msg) {
	super(msg); 
 }
}
