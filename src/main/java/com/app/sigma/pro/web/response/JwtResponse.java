package com.app.sigma.pro.web.response;

import java.io.Serializable;

public class JwtResponse extends BaseResponse implements Serializable{

	private static final long serialVersionUID = -8091879091924046844L;
	private  String jwttoken;
	public String getJwttoken() {
		return jwttoken;
	}
	public void setJwttoken(String jwttoken) {
		this.jwttoken = jwttoken;
	}

	
}