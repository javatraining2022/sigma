package com.app.sigma.pro.web.response;

import com.app.sigma.pro.data.jpa.persistance.Category;

public class CategoryResponse extends BaseResponse {
	private Category category;

	public Category getCategory() {
		return category;
	}

	public void setCategory(Category category) {
		this.category = category;
	}
	

}
