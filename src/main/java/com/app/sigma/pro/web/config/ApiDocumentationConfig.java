package com.app.sigma.pro.web.config;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;

import com.app.sigma.pro.data.constant.RTConstants;

import io.swagger.annotations.ApiKeyAuthDefinition;
import io.swagger.annotations.SwaggerDefinition;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.ApiKey;
import springfox.documentation.service.Contact;
import springfox.documentation.service.SecurityScheme;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;

/**
 * Configuration file for Swagger's documentation.
 *
 * @author Jayakrishna Marripudi
 */
@Configuration
public class ApiDocumentationConfig {
    /** Security definition API Key name. */
    private static final String SECURITY_DEFINITION_API_KEY_NAME = "Bearer";

    @Value("${swagger.api-documentation.title}")
	private String title;
    @Value("${swagger.api-documentation.description}")
	private String description;
    @Value("${swagger.api-documentation.contact.name}")
	private String name;
    @Value("${swagger.api-documentation.contact.email}")
	private String email;
   

    /**
     * Builds and returns an {@link ApiInfo} object.
     *
     * @return Instance of type {@link ApiInfo} that holds the API documentation metadata.
     */
    public ApiInfo apiInfo() {
       

        return new ApiInfoBuilder().title(title)
                .description(description)
               
               
                .contact(new Contact(name,"",email))
                .build();
    }

    @Bean
    public Docket customImplementation() {
    	
        return new Docket(DocumentationType.SWAGGER_2)
        		.select()
                .apis(RequestHandlerSelectors.basePackage(RTConstants.BASE_PACKAGE))//select the controllers to which documentation requires
        		.build()
                .apiInfo(apiInfo())
                .protocols(protocols())
                .produces(requestResponsePayloadFormats())
                .consumes(requestResponsePayloadFormats())
                .securitySchemes(securitySchemes());
    }

    // ---------------
    // Private Methods
    // ---------------

    /**
     * Returns the list of global security schemes.
     *
     * @return Collection of global security schemes.
     */
    private List<? extends SecurityScheme> securitySchemes() {
        final List<SecurityScheme> schemes = new ArrayList<>();
        schemes.add(new ApiKey(ApiDocumentationConfig.SECURITY_DEFINITION_API_KEY_NAME, HttpHeaders.AUTHORIZATION, ApiKeyAuthDefinition.ApiKeyLocation.HEADER.toValue()));
        return schemes;
    }

    /**
     * Returns a set of supported protocols.
     *
     * @return Collection of supported protocols.
     */
    private Set<String> protocols() {
        final Set<String> protocols = new HashSet<>();
        protocols.add(SwaggerDefinition.Scheme.HTTP.name()
                              .toLowerCase());
       
        return protocols;
    }

    /**
     * Returns a set of supported request, response payload formats (i.e. produces and consumes).
     *
     * @return Collection of supported payload formats.
     */
    private Set<String> requestResponsePayloadFormats() {
        final Set<String> payloadFormats = new HashSet<>();
        payloadFormats.add(MediaType.APPLICATION_JSON_VALUE);
        return payloadFormats;
    }

}
