/*
 * Copyright (c) 2020 Innominds Software Pvt Ltd.. All rights reserved.
 *
 * This file is part of Emergency Crowd Control and Service Provision.
 *
 * Emergency Crowd Control and Service Provision project and associated code cannot be copied
 * and/or distributed without a written permission of Innominds Software Pvt Ltd.,
 * and/or its subsidiaries.
 */
package com.app.sigma.pro.web.controller;

import java.io.IOException;

import org.springframework.util.StringUtils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * Utility class that provides helper methods to serialize / de-serialize objects from / to JSON format.
 *
 * @author Sunil Kumar Dhage
 */
public final class JsonUtils {
    /**
     * Private constructor.
     */
    private JsonUtils() {
        throw new IllegalStateException("Cannot create instances of this class");
    }

    /**
     * This method serializes the provided payload to a JSON string. If an exception occurs, it is suppressed and
     * instead returns an empty string.
     *
     * @param payload
     *         Payload that needs to be serialized.
     *
     * @return Serialized JSON string or an empty string if any exceptions occur.
     */
    public static String serialize(final Object payload) {
        String serializedString;
        try {
            serializedString = new ObjectMapper().writeValueAsString(payload);
        } catch (final JsonProcessingException e) {
            serializedString = "";
        }
        return serializedString;
    }

    /**
     * This method serializes the provided payload to a JSON string. If an exception occurs, it is thrown back to the
     * caller.
     *
     * @param payload
     *         Payload that needs to be serialized.
     *
     * @return Serialized JSON string.
     */
    public static String serializeOrThrow(final Object payload) {
        try {
            return new ObjectMapper().writeValueAsString(payload);
        } catch (final JsonProcessingException e) {
           
        }
		return null;
    }

    /**
     * This method attempts to deserialize the provided payload to the specified target type.
     *
     * @param payload
     *         Payload that needs to be deserialized.
     * @param targetType
     *         Target type.
     * @param <T>
     *         Target type that the payload needs to be deserialized to.
     *
     * @return Instance of type {@code targetType}.
     */
    public static <T> T deserialize(final String payload, final Class<T> targetType) {
        try {
            return new ObjectMapper().readValue(payload, targetType);
        } catch (final IOException e) {
            
        }
        return null;
    }
}
