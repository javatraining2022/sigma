package com.app.sigma.pro.web.response;

import java.util.List;

import com.app.sigma.pro.data.jpa.model.RTError;


public class BaseResponse {
	private String status;
	
	private List<RTError> errors;

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public List<RTError> getErrors() {
		return errors;
	}

	public void setErrors(List<RTError> errors) {
		this.errors = errors;
	}
	

	

}
