package com.app.sigma.pro.web.response;

import java.util.List;

import com.app.sigma.pro.data.jpa.persistance.Category;

public class CategoriesResponse {
	private List<Category> categories;

	public List<Category> getCategories() {
		return categories;
	}

	public void setCategories(List<Category> categories) {
		this.categories = categories;
	}
	
}
