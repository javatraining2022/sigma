package com.app.sigma.pro.web.response;

import com.app.sigma.pro.data.jpa.persistance.UserSubscription;
public class UserSubscriptionResponse extends BaseResponse {
private UserSubscription userSubscription;

public UserSubscription getUserSubscription() {
	return userSubscription;
}

public void setUserSubscription(UserSubscription userSubscription) {
	this.userSubscription = userSubscription;
}

}
