package com.app.sigma.pro.web.response;

import java.util.List;

import com.app.sigma.pro.data.jpa.persistance.Subscription;

public class SubscriptionsResponse extends BaseResponse {
	private List<Subscription> subscriptions;

	public List<Subscription> getSubscriptions() {
		return subscriptions;
	}

	public void setSubscriptions(List<Subscription> subscriptions) {
		this.subscriptions = subscriptions;
	}
	
	
}
