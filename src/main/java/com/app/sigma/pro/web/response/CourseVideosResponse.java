package com.app.sigma.pro.web.response;

import java.util.List;

import com.app.sigma.pro.data.jpa.persistance.CourseVideo;

public class CourseVideosResponse extends BaseResponse{
	private List<CourseVideo> courseVideos;

	public List<CourseVideo> getCourseVideos() {
		return courseVideos;
	}

	public void setCourseVideos(List<CourseVideo> courseVideos) {
		this.courseVideos = courseVideos;
	}
	
}
