package com.app.sigma.pro.web.controller;

import java.io.File;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.app.sigma.pro.data.request.UpdateRequest;
import com.app.sigma.pro.data.request.UserRequest;
import com.app.sigma.pro.data.service.UserService;
import com.app.sigma.pro.web.response.UserResponse;
import com.app.sigma.pro.web.response.UsersResponse;
import com.fasterxml.jackson.databind.ObjectMapper;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.Authorization;

@Api(value = UserController.API_CATEGORY, tags = { UserController.API_TAGS })
@RestController
@RequestMapping("/api")
@CrossOrigin

public class UserController {
	/** Category to which these APIs belong. */
	public static final String API_CATEGORY = "Sigma Pro User";

	/** Tags to be associated to identify these APIs. */
	public static final String API_TAGS = "Sigma Pro User";
	@Autowired
	private UserService rtUserService;

	
	@ApiOperation(value = "This API is used to fetch the User details.", nickname = "findUser", tags = {
			UserController.API_TAGS }, authorizations = { @Authorization(value = "Bearer") })
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Successfully fetched the User Details.") })
	@GetMapping("/sigma")
	public ResponseEntity<UserResponse> findUser(@RequestParam long id) {
		UserResponse response = rtUserService.findUser(id);
		return ResponseEntity.ok(response);
	}
	@ApiOperation(value = "This API is used to fetch the User details By user Name.", nickname = "findUserByUserName", tags = {
			UserController.API_TAGS }, authorizations = { @Authorization(value = "Bearer") })
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Successfully fetched the User Details.") })
	@GetMapping("/sigma/username")
	public ResponseEntity<UserResponse> findUserByUserName(@RequestParam String name) {
		UserResponse response = rtUserService.findUserByUsername(name);
		return ResponseEntity.ok(response);
	}
	
	@ApiOperation(value = "This API is used to fetch the all User's details.", nickname = "findAllUsers", tags = {
			UserController.API_TAGS }, authorizations = { @Authorization(value = "Bearer") })
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Successfully fetched the all User's Details.") })
	@GetMapping("/sigma/all/users")
	public ResponseEntity<UsersResponse> findAllUsers() {
		UsersResponse response = rtUserService.findAllUsers();
		return ResponseEntity.ok(response);
	}
	@ApiOperation(value = "This API is used to fetch the all Tutors details.", nickname = "findAllTutors", tags = {
			UserController.API_TAGS }, authorizations = { @Authorization(value = "Bearer") })
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Successfully fetched the all Tutor's Details.") })
	@GetMapping("/sigma/all/tutors")
	public ResponseEntity<UsersResponse> findAllTutors() {
		UsersResponse response = rtUserService.findAllTutors();
		return ResponseEntity.ok(response);
	}
	@ApiOperation(value = "This API is used to fetch the all Tutor's details elaed  to Category.", nickname = "findAllTutorsByCategory", tags = {
			UserController.API_TAGS }, authorizations = { @Authorization(value = "Bearer") })
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Successfully fetched the all Tutors Details Related to specific category.") })
	@GetMapping("/sigma/all/categorytutors")
	public ResponseEntity<UsersResponse> findAllTutorsByCategory(@RequestParam String category) {
		UsersResponse response = rtUserService.findAllTutorsByCategory(category);
		return ResponseEntity.ok(response);
	}

	@ApiOperation(value = "This API is used to update the  User's details.", nickname = "updateUser", tags = {
			UserController.API_TAGS }, authorizations = { @Authorization(value = "Bearer") })
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Successfully updated the User's Details.") })
	@PutMapping("/sigma/modifyuser")
	public ResponseEntity<UserResponse> updateUser(@RequestBody UpdateRequest user)
			throws Exception {
		UserResponse response = rtUserService.updateUser(user);
		return ResponseEntity.ok(response);

	}
	
	@ApiOperation(value = "This API is used to Delete User.", nickname = "deleteUser", tags = {
			UserController.API_TAGS }, authorizations = { @Authorization(value = "Bearer") })
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Successfully deleted the User.") })
	@DeleteMapping("/sigma/removeuser")
	public ResponseEntity<?> deleteUser(@RequestParam long id) throws Exception {
		return ResponseEntity.ok(rtUserService.deleteUser(id));
	}
}
