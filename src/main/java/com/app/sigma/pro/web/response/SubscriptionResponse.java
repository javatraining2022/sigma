package com.app.sigma.pro.web.response;

import com.app.sigma.pro.data.jpa.persistance.Subscription;

public class SubscriptionResponse extends BaseResponse {

	private Subscription subscription;

	public Subscription getSubscription() {
		return subscription;
	}

	public void setSubscription(Subscription subscription) {
		this.subscription = subscription;
	}
	

}
