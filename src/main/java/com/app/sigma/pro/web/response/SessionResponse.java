package com.app.sigma.pro.web.response;

import com.app.sigma.pro.data.jpa.persistance.Session;

public class SessionResponse extends BaseResponse {
	private Session session;

	public Session getSession() {
		return session;
	}

	public void setSession(Session session) {
		this.session = session;
	}
	
	
}
