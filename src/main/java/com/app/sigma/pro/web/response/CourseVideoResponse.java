package com.app.sigma.pro.web.response;

import com.app.sigma.pro.data.jpa.persistance.CourseVideo;

public class CourseVideoResponse extends BaseResponse {
private CourseVideo courseVideo;

public CourseVideo getCourseVideo() {
	return courseVideo;
}

public void setCourseVideo(CourseVideo courseVideo) {
	this.courseVideo = courseVideo;
}

}
