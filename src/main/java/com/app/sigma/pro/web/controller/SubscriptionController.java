package com.app.sigma.pro.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.app.sigma.pro.data.request.CreateSubscriptionRequest;
import com.app.sigma.pro.data.request.CreateUserSubscriptionRequest;
import com.app.sigma.pro.data.service.UserSubscriptionService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.Authorization;

@Api(value = SubscriptionController.API_CATEGORY, tags = { SubscriptionController.API_TAGS })
@RestController
@RequestMapping("/api/sigma")
@CrossOrigin
public class SubscriptionController {
	
	@Autowired
	private UserSubscriptionService userSubscriptionService;
	
	
	/** Category to which these APIs belong. */
	public static final String API_CATEGORY = "Sigma Pro Subscriptions";

	/** Tags to be associated to identify these APIs. */
	public static final String API_TAGS = "Sigma Pro Subscriptions";

	@ApiOperation(value = "This API is used to Create the Subscription.", nickname = "createSubscription", tags = {
			SubscriptionController.API_TAGS }, authorizations = { @Authorization(value = "Bearer") })
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Subscription Created SuccessFully.") })
	@PostMapping(value="/subscription/create")
	public ResponseEntity<?> createSubscription(@Validated @RequestBody CreateSubscriptionRequest request) throws Exception {
		return ResponseEntity.ok(userSubscriptionService.createSubscription(request));
	}
	
	@ApiOperation(value = "This API is used to get the Subsccriptions.", nickname = "getAllSubsccriptions", tags = {
			SubscriptionController.API_TAGS }, authorizations = { @Authorization(value = "Bearer") })
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Subscriptions fetched SuccessFully.") })
	@GetMapping(value="/subscription/all")
	public ResponseEntity<?> getAllSubsccriptions() throws Exception {
		return ResponseEntity.ok(userSubscriptionService.fetchAllSubscriptions());
	}
	
	@ApiOperation(value = "This API is used to Create Subscription for the User.", nickname = "createUserSubscription", tags = {
			SubscriptionController.API_TAGS }, authorizations = { @Authorization(value = "Bearer") })
	@ApiResponses(value = { @ApiResponse(code = 200, message = "UserSubscription Created SuccessFully.") })
	@PostMapping(value="/userSubscription/create")
	public ResponseEntity<?> createUserSubscription(@Validated @RequestBody CreateUserSubscriptionRequest request) throws Exception {
		return ResponseEntity.ok(userSubscriptionService.createUserSubscription(request));
	}
	
	@ApiOperation(value = "This API is used to get the Subscriptions of User.", nickname = "getUserSubscriptions", tags = {
			SubscriptionController.API_TAGS }, authorizations = { @Authorization(value = "Bearer") })
	@ApiResponses(value = { @ApiResponse(code = 200, message = "UserSubscriptions fetched SuccessFully.") })
	@GetMapping(value="/usersubscriptions")
	public ResponseEntity<?> getUserSubscriptions(@RequestParam long subscriberId) throws Exception {
		return ResponseEntity.ok(userSubscriptionService.fetchUserSubscriptions(subscriberId));
	}
	
	
}
