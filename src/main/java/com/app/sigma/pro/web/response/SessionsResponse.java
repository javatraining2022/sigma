package com.app.sigma.pro.web.response;

import java.util.List;

import com.app.sigma.pro.data.jpa.persistance.Session;

public class SessionsResponse extends BaseResponse {
List<Session> sessions;

public List<Session> getSessions() {
	return sessions;
}

public void setSessions(List<Session> sessions) {
	this.sessions = sessions;
}

}
