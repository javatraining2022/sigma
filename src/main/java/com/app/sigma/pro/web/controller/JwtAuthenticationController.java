package com.app.sigma.pro.web.controller;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.amazonaws.AmazonServiceException;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.app.sigma.pro.data.constant.RTConstants;
import com.app.sigma.pro.data.jpa.model.RTError;
import com.app.sigma.pro.data.jpa.persistance.Role;
import com.app.sigma.pro.data.jpa.persistance.User;
import com.app.sigma.pro.data.repository.UserRepository;
import com.app.sigma.pro.data.request.JwtRequest;
import com.app.sigma.pro.data.request.UserRequest;
import com.app.sigma.pro.data.service.JwtUserDetailsService;
import com.app.sigma.pro.data.service.UserService;
import com.app.sigma.pro.data.util.JwtTokenUtil;
import com.app.sigma.pro.web.exception.SigmaProCommonException;
import com.app.sigma.pro.web.response.BaseResponse;
import com.app.sigma.pro.web.response.JwtResponse;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@Api(value = JwtAuthenticationController.API_CATEGORY, tags = { JwtAuthenticationController.API_TAGS })
@RestController
@RequestMapping("/api")
@CrossOrigin
public class JwtAuthenticationController {
	@Autowired
	private AmazonS3 s3client;
	private String fileUrl;
	private String status;
	/** Category to which these APIs belong. */
	public static final String API_CATEGORY = "Sigma Pro User Authentication";

	/** Tags to be associated to identify these APIs. */
	public static final String API_TAGS = "Sigma Pro User Authentication";
	@Autowired
	private AuthenticationManager authenticationManager;

	@Autowired
	private JwtTokenUtil jwtTokenUtil;

	@Autowired
	private JwtUserDetailsService userDetailsService;

	@Autowired
	private UserService rtUserService;

	@Autowired
	private UserRepository userRepo;
	@Autowired
	private PasswordEncoder bcryptEncoder;


	@ApiOperation(value = "This API is used to authenticte the User and Provides Token.", nickname = "createAuthenticationToken", tags = {
			JwtAuthenticationController.API_TAGS })
	@ApiResponses(value = { @ApiResponse(code = 200, message = "User Authenticated SuccessFully.") })
	@PostMapping("/sigma/login")
	public ResponseEntity<?> createAuthenticationToken(@Validated @RequestBody JwtRequest authenticationRequest) throws Exception {
		List<RTError> errors= validateRequest(authenticationRequest.getUsername(),
				authenticationRequest.getPassword());
		JwtResponse response=new JwtResponse();

		if(errors!=null) {
			response.setErrors(errors);
			return ResponseEntity.ok(response);
		}
		try {
		authenticate(authenticationRequest.getUsername(), authenticationRequest.getPassword());
		final UserDetails userDetails = userDetailsService.loadUserByUsername(authenticationRequest.getUsername());

		final String token = jwtTokenUtil.generateToken(userDetails);
		response.setJwttoken(token);
		response.setStatus("SUCCESS");
		}catch(Exception e) {
			throw new SigmaProCommonException("Invalid Credentials");
		}
		
		
		
		
		
		return ResponseEntity.ok(response);
		}

	private List<RTError> validateRequest(String username, String password) {
		
		User user = userRepo.findByUserName(username);
		List<RTError> errors=null;
		if (user == null) {
			throw new SigmaProCommonException("User Details Not Found");
			
		}
		return errors;

	}

	@ApiOperation(value = "This API is used to Register the User.", nickname = "saveUser", tags = {
			JwtAuthenticationController.API_TAGS })
	@ApiResponses(value = { @ApiResponse(code = 200, message = "User Registered SuccessFully.") })
	@PostMapping(value = "/sigma/register")
	public ResponseEntity<?> saveUser(@Validated @RequestBody UserRequest user) throws Exception {

		return ResponseEntity.ok(rtUserService.saveUser(user));
	}

	@ApiOperation(value = "This API is used to Upload the User Profilepic.", nickname = "uploadPic", tags = {
			JwtAuthenticationController.API_TAGS })
	@ApiResponses(value = { @ApiResponse(code = 200, message = "User Pic Uploaded SuccessFully.") })
	@PostMapping(value = "/sigma/upload", consumes = { MediaType.MULTIPART_FORM_DATA_VALUE })
	public ResponseEntity<?> uploadPic(@RequestPart MultipartFile pic) throws Exception {

		String fileUrl = "";
		String status = null;
		try {
			// converting multipart file to file
			File file = convertMultiPartToFile(pic);
			// filename
			String fileName = pic.getOriginalFilename();
			fileUrl = "https://s3.us-east-2.amazonaws.com" + "/" + "sigmapropics" + "/" + fileName;
			status = uploadFileTos3bucket(fileName, file);
			file.delete();
		} catch (Exception e) {

		}
		JwtResponse response=new JwtResponse();
		response.setJwttoken(fileUrl);

		return ResponseEntity.ok(response);
	}

	private File convertMultiPartToFile(final MultipartFile multipartFile) {
		final File file = new File(multipartFile.getOriginalFilename());
		try (final FileOutputStream outputStream = new FileOutputStream(file)) {
			outputStream.write(multipartFile.getBytes());
		} catch (final IOException ex) {

		}
		return file;
	}

	private String uploadFileTos3bucket(String fileName, File file) {
		try {
			s3client.putObject(new PutObjectRequest("sigmapropics", fileName, file));
		} catch (AmazonServiceException e) {
			return "uploadFileTos3bucket().Uploading failed :" + e.getMessage();
		}
		return "Uploading Successfull -> ";
	}

	@ApiOperation(value = "This API is used to add New Roles.", nickname = "addRole", tags = {
			JwtAuthenticationController.API_TAGS })
	@ApiResponses(value = { @ApiResponse(code = 200, message = "New Role Added SuccessFully.") })
	@PostMapping("/sigma/role")
	public ResponseEntity<?> addRole(@Validated @RequestBody Role role) throws Exception {
		return ResponseEntity.ok(rtUserService.addRole(role));
	}

	private void authenticate(String username, String password) throws Exception {
		try {
			authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, password));

		} catch (DisabledException e) {
			throw new DisabledException("User Disabled");
		} catch (BadCredentialsException e) {
			throw new BadCredentialsException("Invalid Credentials");
		}
	}
}
