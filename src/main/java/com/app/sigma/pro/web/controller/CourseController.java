package com.app.sigma.pro.web.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.app.sigma.pro.data.jpa.persistance.Category;
import com.app.sigma.pro.data.request.CourseRequest;
import com.app.sigma.pro.data.request.FetchCourseRequest;
import com.app.sigma.pro.data.request.FetchTutorCourseRequest;
import com.app.sigma.pro.data.service.CourseService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.Authorization;

@Api(value = CourseController.API_CATEGORY, tags = { CourseController.API_TAGS })
@RestController
@RequestMapping("/api/sigma")
@CrossOrigin
public class CourseController {
	@Autowired
	private CourseService courseService;

	private static final Logger LOGGER = LoggerFactory.getLogger(CourseController.class);

	/** Category to which these APIs belong. */
	public static final String API_CATEGORY = "Sigma Pro Course";

	/** Tags to be associated to identify these APIs. */
	public static final String API_TAGS = "Sigma Pro Course";

	@ApiOperation(value = "This API is used to Create a new Course.", nickname = "createCourse", tags = {
			CourseController.API_TAGS }, authorizations = { @Authorization(value = "Bearer") })
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Course Created SuccessFully.")
	/*
	 * @ApiResponse(code = 403, message =
	 * "You do not have permissions to perform this operation.")
	 */ })
	@PostMapping("/course/create")
	public ResponseEntity<?> createCourse(@Validated @RequestBody CourseRequest course) throws Exception {
		return ResponseEntity.ok(courseService.createCourse(course));
	}

	@ApiOperation(value = "This API is used to Create a new Category.", nickname = "createCategory", tags = {
			CourseController.API_TAGS })
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Category Created SuccessFully.") })
	@PostMapping("/category/create")
	public ResponseEntity<?> createCategory(@Validated @RequestBody Category category) throws Exception {
		return ResponseEntity.ok(courseService.createCategory(category));
	}

	@ApiOperation(value = "This API is used to fetch All Categories.", nickname = "fetchCategories", tags = {
			CourseController.API_TAGS })
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Fetched all Categories SuccessFully.") })
	@GetMapping("/categories/all")
	public ResponseEntity<?> fetchCategories() throws Exception {
		return ResponseEntity.ok(courseService.allCategories());
	}

	@ApiOperation(value = "This API is used to fetch Course Details.", nickname = "fetchCourse", tags = {
			CourseController.API_TAGS }, authorizations = { @Authorization(value = "Bearer") })
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Fetched Course SuccessFully.") })
	@GetMapping("/course")
	public ResponseEntity<?> fetchCourse(@RequestParam long id) throws Exception {
		return ResponseEntity.ok(courseService.findCourse(id));
	}

	@ApiOperation(value = "This API is used to fetch Courses Details.", nickname = "fetchCourses", tags = {
			CourseController.API_TAGS }, authorizations = { @Authorization(value = "Bearer") })
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Fetched Courses SuccessFully.") })
	@PostMapping("/courses/all")
	public ResponseEntity<?> fetchCourses() throws Exception {
		return ResponseEntity.ok(courseService.allCourses());
	}

	@ApiOperation(value = "This API is used to fetch Courses Details of a tutor.", nickname = "fetchTutorCourses", tags = {
			CourseController.API_TAGS }, authorizations = { @Authorization(value = "Bearer") })
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Fetched Tutor Courses SuccessFully.") })
	@PostMapping("/tutorcourses")
	public ResponseEntity<?> fetchTutorCourses(@Validated @RequestBody FetchTutorCourseRequest req) throws Exception {
		return ResponseEntity.ok(courseService.getCoursesofTutor(req.getCategory(), req.getUserId()));
	}

	@ApiOperation(value = "This API is used to fetch Courses Details By category.", nickname = "fetchCoursesByCategory", tags = {
			CourseController.API_TAGS }, authorizations = { @Authorization(value = "Bearer") })
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Fetched Courses SuccessFully.") })
	@PostMapping("/courses")
	public ResponseEntity<?> fetchCoursesByCategory(@Validated @RequestBody FetchCourseRequest req) throws Exception {
		return ResponseEntity.ok(courseService.getCourses(req.getCategory()));
	}

	@ApiOperation(value = "This API is used to fetch Courses Details of a Course by Course RecgId.", nickname = "fetchCourseByRecgId", tags = {
			CourseController.API_TAGS }, authorizations = { @Authorization(value = "Bearer") })
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Fetched  Course SuccessFully.") })
	@GetMapping("/courseByRecgId")
	public ResponseEntity<?> fetchCourseByRecgId(@RequestParam String id) throws Exception {
		return ResponseEntity.ok(courseService.getCoursesByRecgId(id));
	}

}
