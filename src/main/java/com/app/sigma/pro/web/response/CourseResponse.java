package com.app.sigma.pro.web.response;

import com.app.sigma.pro.data.jpa.persistance.Course;

public class CourseResponse extends BaseResponse {
	private Course course;

	public Course getCourse() {
		return course;
	}

	public void setCourse(Course course) {
		this.course = course;
	}
	

}
