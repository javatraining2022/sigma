package com.app.sigma.pro.data.service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import com.app.sigma.pro.data.constant.RTConstants;
import com.app.sigma.pro.data.jpa.model.RTError;
import com.app.sigma.pro.data.jpa.persistance.Role;
import com.app.sigma.pro.data.jpa.persistance.User;
import com.app.sigma.pro.data.repository.RoleRepository;
import com.app.sigma.pro.data.repository.SessionRepository;
import com.app.sigma.pro.data.repository.UserRepository;
import com.app.sigma.pro.data.request.UpdateRequest;
import com.app.sigma.pro.data.request.UserRequest;
import com.app.sigma.pro.web.exception.SigmaProCommonException;
import com.app.sigma.pro.web.response.BaseResponse;
import com.app.sigma.pro.web.response.UserResponse;
import com.app.sigma.pro.web.response.UsersResponse;

@Service
public class UserService {
	private static final Logger LOGGER = LoggerFactory.getLogger(UserService.class);

	private UserRepository rtUserRepository;
	private SessionRepository rtePassRepository;
	private RoleRepository roleRepository;

	private PasswordEncoder bcryptEncoder;

	public UserService(RoleRepository roleRepository, UserRepository rtUserRepository,
			SessionRepository rtePassRepository, PasswordEncoder bcryptEncoder) {
		this.rtUserRepository = rtUserRepository;
		this.rtePassRepository = rtePassRepository;
		this.roleRepository = roleRepository;
		this.bcryptEncoder = bcryptEncoder;
	}

	public UsersResponse findAllUsers() {
		LOGGER.info("Started Fetching All users.");
		UsersResponse response = new UsersResponse();
		List<User> all = rtUserRepository.findAll();
		if(!CollectionUtils.isEmpty(all)) {
			
		List<User> users = all.stream().filter(user -> user.getRoles().contains(getRoleDetails("SIGMA_USER")))
				.collect(Collectors.toList());
		response.setStatus(RTConstants.SUCCESS);
		response.setUsers(users);
		}
		return response;
	}

	public UsersResponse findAllTutors() {
		LOGGER.info("Started Fetching All Tutors.");
		UsersResponse response = new UsersResponse();
		List<User> all = rtUserRepository.findAll();
		if(!CollectionUtils.isEmpty(all)) {
		List<User> users = all.stream().filter(
				user -> user.getRoles().contains(getRoleDetails("SIGMA_TUTOR")))
				.collect(Collectors.toList());
		response.setStatus(RTConstants.SUCCESS);
		response.setUsers(users);
		}
		return response;
	}
	public UsersResponse findAllTutorsByCategory(String category) {
		LOGGER.info("Started Fetching All Tutors.");
		UsersResponse response = new UsersResponse();
		List<User> all = rtUserRepository.findAll();
		if(!CollectionUtils.isEmpty(all)) {
		List<User> users = all.stream().filter(
				user -> user.getRoles().contains(getRoleDetails("SIGMA_TUTOR")))
				.collect(Collectors.toList());
		response.setStatus(RTConstants.SUCCESS);
		response.setUsers(users);
		}
		return response;
	}

	private boolean validateUser(String username) {
		User user = rtUserRepository.findByUserName(username);
		if (user != null) {
			return true;
		}
		return false;
	}

	public UserResponse saveUser(final UserRequest user) {
		LOGGER.info("Started Saving new user.");
		List<RTError> errors = new ArrayList<>();
		UserResponse response = new UserResponse();
		if (validateUser(user.getUserName())) {
			RTError error = new RTError();
			error.setName("User Already Exists");
			error.setDescription("User Exists!");
			errors.add(error);
			response.setErrors(errors);
			response.setStatus(RTConstants.ALREADY_EXISTS);
			return response;

		}
		User newUser = new User();
		Role role = roleRepository.findByName(user.getRole());
		Set<Role> roles = new HashSet<>();
		if (role != null && !StringUtils.isEmpty(role.getName())) {

			roles.add(role);
			newUser.setRoles(roles);
		}

		newUser.setCountryCode(user.getCountryCode());
		newUser.setEmail(user.getEmail());
		newUser.setPassword(bcryptEncoder.encode(user.getPassword()));
		newUser.setPhone(user.getPhone());
		newUser.setUserName(user.getUserName());
		newUser.setImageUrl(user.getUrl());
		newUser.setGender(user.getGender());
		newUser.setCategory(user.getCategory());
		User savedUser = null;
		try {
			savedUser = rtUserRepository.save(newUser);
		} catch (Exception e) {
			RTError error = new RTError();
			error.setName("Some Thing Went Wrong");
			error.setDescription("Please Try Again!");
			errors.add(error);
			response.setErrors(errors);
		}
		LOGGER.info("Completed Saving new user/tutor.");
		response.setStatus(RTConstants.SUCCESS);
		response.setUser(savedUser);
		return response;
	}

	
	public UserResponse updateUser(final UpdateRequest user) {
		LOGGER.info("Started Updating existng user/tutor.");
		UserResponse response = new UserResponse();
		Optional<User> newUser = rtUserRepository.findById(user.getId());
		User currentUser=new User();
		currentUser.setId(user.getId());
		currentUser.setCountryCode("+91");
		currentUser.setCategory(newUser.get().getCategory());
		currentUser.setEmail(user.getEmail());
		currentUser.setPhone(user.getPhone());
		currentUser.setRoles(newUser.get().getRoles());
		currentUser.setUserName(newUser.get().getUserName());
		currentUser.setPassword(newUser.get().getPassword());
		User savedUser = null;
		List<RTError> errors = new ArrayList<>();
		try {
			savedUser = rtUserRepository.save(currentUser);
		} catch (Exception e) {
			RTError error = new RTError();
			error.setName("Some Thing Went Wrong");
			error.setDescription("Please Try Again!");
			errors.add(error);
			response.setErrors(errors);
		}

		LOGGER.info("Completed Updating existing user/tutor.");
		response.setStatus(RTConstants.SUCCESS);

		response.setUser(savedUser);
		return response;
	}

	public UserResponse findUser(final long id) {
		LOGGER.info("Started fetching existing user.");
		UserResponse response = new UserResponse();
		Optional<User> user = rtUserRepository.findById(id);
		User savedUser = user.get();
       if(user.get()==null) {
    	  throw new SigmaProCommonException("User Details Not Found"); 
       }
		LOGGER.info("Completed fetching existing user.");
		response.setStatus(RTConstants.SUCCESS);
		response.setUser(savedUser);
		return response;
	}

	public UserResponse findUserByUsername(final String name) {
		LOGGER.info("Started fetching existing user.");
		UserResponse response = new UserResponse();
		User user = rtUserRepository.findByUserName(name);

		/*
		 * UserRequest rtUser =
		 * UserRequest.builder().category(savedUser.getCategory()).id(savedUser.getId())
		 * .userName(savedUser.getUserName())
		 * .password("**********").phone(savedUser.getPhone()).countryCode(savedUser.
		 * getCountryCode()) .email(savedUser.getEmail()).build();
		 */
		LOGGER.info("Completed fetching existing user.");
		response.setStatus(RTConstants.SUCCESS);
		response.setUser(user);
		return response;
	}

	public BaseResponse deleteUser(final long id) {
		BaseResponse response = new BaseResponse();
		rtUserRepository.deleteById(id);
		response.setStatus(RTConstants.SUCCESS);
		return response;
	}

	public BaseResponse addRole(final Role role) {
		BaseResponse response = new BaseResponse();
		if (roleRepository.findByName(role.getName()) != null) {
			response.setStatus(RTConstants.ALREADY_EXISTS);

			return response;
		}
		roleRepository.save(role);
		response.setStatus(RTConstants.SUCCESS);
		return response;
	}

	private Role getRoleDetails(String name) {
		Role role = roleRepository.findByName(name);
		return role;
	}

}
