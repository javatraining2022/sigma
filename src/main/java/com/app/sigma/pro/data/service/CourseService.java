package com.app.sigma.pro.data.service;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.app.sigma.pro.data.jpa.persistance.Category;
import com.app.sigma.pro.data.jpa.persistance.Course;
import com.app.sigma.pro.data.repository.CategoryRepository;
import com.app.sigma.pro.data.repository.CourseRepository;
import com.app.sigma.pro.data.request.CourseRequest;
import com.app.sigma.pro.web.response.CategoryResponse;
import com.app.sigma.pro.web.response.CourseResponse;
import com.app.sigma.pro.web.response.CoursesResponse;

@Service
public class CourseService {

	private CourseRepository courseRepository;
	private CategoryRepository categoryRepository;

	private static final Logger LOGGER = LoggerFactory.getLogger(CourseService.class);

	public CourseService(CourseRepository courseRepository,CategoryRepository categoryRepository) {
		this.courseRepository = courseRepository;
		this.categoryRepository=categoryRepository;
	}

	@Transactional
	public CourseResponse createCourse(final CourseRequest req) {
		LOGGER.info("Creating the Course.");
		CourseResponse response = new CourseResponse();
		Course course = new Course();
		course.setCategory(req.getCategory());
		course.setDescription(req.getDescription());
		
		course.setDurationDays(req.getDuration());
		course.setName(req.getName());
		course.setUserId(req.getTutorId());
		course.setCost(req.getCost());
		course.setTutorName(req.getTutorName());
		Course savedCourse = courseRepository.save(course);
		savedCourse.setRecgId("SPCRID"+savedCourse.getId());
		Course newSavedCourse = courseRepository.save(savedCourse);
		if (newSavedCourse==null) {

			
			response.setCourse(null);
			response.setStatus("ERROR");
			return response;
		}
		response.setCourse(newSavedCourse);
		response.setStatus("SUCCESS");

		return response;
	}
	
	@Transactional
	public CategoryResponse createCategory(final Category req) {
		CategoryResponse response=new CategoryResponse();
		Category savedCategory = categoryRepository.save(req);
		if (savedCategory==null) {

			
			response.setCategory(null);
			response.setStatus("ERROR");
			return response;
		}
		response.setCategory(savedCategory);
		response.setStatus("SUCCESS");

		return response;
	}

	public List<Category> allCategories() {
		List<Category> categories = categoryRepository.findAll();
		
		return categories;
	}

	public CoursesResponse allCourses() {
		CoursesResponse response = new CoursesResponse();
		List<Course> courses = courseRepository.findAll();
		if (CollectionUtils.isEmpty(courses)) {
						response.setCourses(null);
			response.setStatus("ERROR");
			return response;
		}
		response.setCourses(courseRepository.findAll());
		response.setStatus("SUCCESS");
		return response;
	}

	public CoursesResponse getCourses(String category) {
		CoursesResponse response = new CoursesResponse();
		List<Course> courses = courseRepository.getCoursesByCategory(category);
		if (CollectionUtils.isEmpty(courses)) {
						response.setCourses(null);
			response.setStatus("ERROR");
			return response;
		}
		response.setCourses(courses);
		response.setStatus("SUCCESS");
		return response;
	}
	public CourseResponse getCoursesByRecgId(String id) {
		CourseResponse response = new CourseResponse();
		Course course = courseRepository.findByRecgId(id);
		if(course!=null) {
			response.setCourse(course);
			response.setStatus("SUCCESS");
		}
		response.setCourse(null);
		response.setStatus("ERROR");
		return response;
	}
	public CoursesResponse getCoursesofTutor(String category,Long userId) {
		CoursesResponse response = new CoursesResponse();
		List<Course> courses = courseRepository.getCoursesByCategoryAndUserId(category,userId);
		if (CollectionUtils.isEmpty(courses)) {
						response.setCourses(null);
			response.setStatus("ERROR");
			return response;
		}
		response.setCourses(courses);
		response.setStatus("SUCCESS");
		return response;
	}

	public CourseResponse findCourse(long id) {
		Optional<Course> course = courseRepository.findById(id);
		CourseResponse response = new CourseResponse();
		if (!course.isPresent()) {

			
			response.setCourse(null);
			response.setStatus("ERROR");
			return response;
		}
		response.setCourse(courseRepository.findById(id).get());
		response.setStatus("SUCCESS");

		return response;
	}

}
