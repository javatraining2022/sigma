package com.app.sigma.pro.data.request;

public class SessionRequest {
	private long sessionId;
	private long tutorId;
	private String sessionName;
	private String sessionCourse;
	private String description;
	private long courseId;
	private String status;
	private String broadcastId;
	public long getSessionId() {
		return sessionId;
	}
	public void setSessionId(long sessionId) {
		this.sessionId = sessionId;
	}
	public long getTutorId() {
		return tutorId;
	}
	public void setTutorId(long tutorId) {
		this.tutorId = tutorId;
	}
	public String getSessionName() {
		return sessionName;
	}
	public void setSessionName(String sessionName) {
		this.sessionName = sessionName;
	}
	public String getSessionCourse() {
		return sessionCourse;
	}
	public void setSessionCourse(String sessionCourse) {
		this.sessionCourse = sessionCourse;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public long getCourseId() {
		return courseId;
	}
	public void setCourseId(long courseId) {
		this.courseId = courseId;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getBroadcastId() {
		return broadcastId;
	}
	public void setBroadcastId(String broadcastId) {
		this.broadcastId = broadcastId;
	}
	
	
	
}
