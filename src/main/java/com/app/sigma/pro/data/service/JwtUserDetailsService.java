package com.app.sigma.pro.data.service;

import java.util.HashSet;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.app.sigma.pro.data.repository.UserRepository;
import com.app.sigma.pro.web.exception.SigmaProCommonException;

@Service
public class JwtUserDetailsService implements UserDetailsService {
	@Autowired
	private UserRepository userRepository;

	

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		if(StringUtils.isEmpty(username)) {
		//throw exception	
		}
		com.app.sigma.pro.data.jpa.persistance.User user=userRepository.findByUserName(username);
		if (user.getUserName().equals(username)) {
			return new User(user.getUserName(),user.getPassword(),getAuthorities(user));
		} else {
			throw new SigmaProCommonException("User not found with username: " + username);
		}
	}
	 
	 private Set getAuthorities(com.app.sigma.pro.data.jpa.persistance.User user) {
	        Set authorities = new HashSet<>();
			user.getRoles().forEach(role -> {
	            authorities.add(new SimpleGrantedAuthority("ROLE_" + role.getName()));
			});
			return authorities;
		}



}
