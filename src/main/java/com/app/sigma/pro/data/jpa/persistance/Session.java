package com.app.sigma.pro.data.jpa.persistance;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "session")
public class Session {
	/** Unique identifier of the Session - typically a primary key. */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", nullable = false)
	private Long id;
	
	
	/** Tutor Id of Tutor associated with Session. */
	@Column(name = "tutor_id",nullable=false)
	private Long tutorId;
	
	/** Name of the Session. */
	@Column(name = "name", length = 128)
	private String name;

	/** CourseId of the Session. */
	@Column(name = "course_id")
	private Long courseId;
	
	
	
	/** CourseId of the Session. */
	@Column(name = "broadcast_id")
	private String broadcastId;
	
	/** CourseId of the Session. */
	@Column(name = "status")
	private String status;

	/** Description of the Session. */
	@Column(name = "description", length = 128)
	@JsonIgnore
	private String description;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getTutorId() {
		return tutorId;
	}

	public void setTutorId(Long tutorId) {
		this.tutorId = tutorId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Long getCourseId() {
		return courseId;
	}

	public void setCourseId(Long courseId) {
		this.courseId = courseId;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getBroadcastId() {
		return broadcastId;
	}

	public void setBroadcastId(String broadcastId) {
		this.broadcastId = broadcastId;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	

	

}
