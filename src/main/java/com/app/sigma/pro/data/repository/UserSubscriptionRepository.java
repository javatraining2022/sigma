package com.app.sigma.pro.data.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.app.sigma.pro.data.jpa.persistance.UserSubscription;

@Repository
public interface UserSubscriptionRepository extends JpaRepository<UserSubscription, Long> {
	@Query(value = "SELECT us FROM UserSubscription us WHERE us.subscriberId = :subscriberId")
	List<UserSubscription> getUserSubscription(@Param("subscriberId")long subscriberId);

}
