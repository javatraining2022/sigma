package com.app.sigma.pro.data.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.app.sigma.pro.data.jpa.persistance.Subscription;

@Repository
public interface SubscriptionRepository extends JpaRepository<Subscription, Long> {
	

}
