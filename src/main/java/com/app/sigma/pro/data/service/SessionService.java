package com.app.sigma.pro.data.service;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.app.sigma.pro.data.constant.RTConstants;
import com.app.sigma.pro.data.jpa.persistance.Session;
import com.app.sigma.pro.data.repository.SessionRepository;
import com.app.sigma.pro.data.request.FetchSessionRequest;
import com.app.sigma.pro.data.request.SessionRequest;
import com.app.sigma.pro.web.response.SessionResponse;
import com.app.sigma.pro.web.response.SessionsResponse;

@Service
public class SessionService {

	private SessionRepository sessionRepository;

	private static final Logger LOGGER = LoggerFactory.getLogger(SessionService.class);

	public SessionService(SessionRepository sessionRepository) {
		this.sessionRepository = sessionRepository;
	}

	static enum Status {
		STARTED, INPROGRESS, PAUSED, COMPLETED, RESUMED;
	}

	@Transactional
	public SessionResponse startSession(final SessionRequest req) {
		LOGGER.info("Starting the Session.");
		SessionResponse response = new SessionResponse();
		Session session=new Session();
		session.setCourseId(req.getCourseId());
		session.setDescription(req.getDescription());
		session.setName(req.getSessionName());
		session.setTutorId(req.getTutorId());
		session.setBroadcastId(req.getBroadcastId());
		session.setStatus("STARTED");
		
		Session savedSession =sessionRepository.save(session);
		if (savedSession==null) {

			
			response.setSession(null);
			response.setStatus("ERROR");
			return response;
		}
		response.setSession(savedSession);
		response.setStatus("SUCCESS");
		return response;
	}
	@Transactional
	public SessionsResponse fetchSessions(final long courseId) {
		LOGGER.info("Starting the Session.");
		SessionsResponse response = new SessionsResponse();
		
		List<Session> sessions=sessionRepository.getSessions(courseId);
		if(!sessions.isEmpty()) {
		response.setSessions(sessions);
		response.setStatus("SUCCESS");
		}else{
			response.setSessions(null);
			response.setStatus(RTConstants.DATA_NOT_FOUND);	
		}
		return response;
	}

	@Transactional
	public SessionResponse endSession(Long sessionId) {
		LOGGER.info("Starting the Session.");
		SessionResponse response = new SessionResponse();
		
		Optional<Session> session=sessionRepository.findById(sessionId);
		session.get().setStatus("COMPLETED");
		Session updatedSession=sessionRepository.save(session.get());
		if(updatedSession!=null) {
		response.setSession(updatedSession);
		response.setStatus("SUCCESS");
		}else{
			response.setSession(null);
			response.setStatus(RTConstants.DATA_NOT_FOUND);	
		}
		return response;
	}

	
}
