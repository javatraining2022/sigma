package com.app.sigma.pro.data.jpa.persistance;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name="coursevideo")
public class CourseVideo {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", nullable = false)
	private Long id;
	
	
	/** Tutor Id of Tutor associated with CourseVideo. */
	@Column(name = "tutor_id",nullable=false)
	private Long tutorId;
	
	/** Name of the CourseVideo. */
	@Column(name = "name", length = 128)
	private String name;

	/** CourseId of the CourseVideo. */
	@Column(name = "course_id")
	private Long courseId;
	
	
	
	/** CourseId of the CourseVideo. */
	@Column(name = "url")
	private String url;
	
	/** CourseId of the CourseVideo. */
	@Column(name = "type")
	private String type;

	/** Description of the CourseVideo. */
	@Column(name = "description", length = 128)
	@JsonIgnore
	private String description;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getTutorId() {
		return tutorId;
	}

	public void setTutorId(Long tutorId) {
		this.tutorId = tutorId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Long getCourseId() {
		return courseId;
	}

	public void setCourseId(Long courseId) {
		this.courseId = courseId;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
	
}
