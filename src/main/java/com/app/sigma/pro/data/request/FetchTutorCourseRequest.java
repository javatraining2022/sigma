package com.app.sigma.pro.data.request;

public class FetchTutorCourseRequest {
 private String category;
 private Long userId;
public String getCategory() {
	return category;
}
public void setCategory(String category) {
	this.category = category;
}
public Long getUserId() {
	return userId;
}
public void setUserId(Long userId) {
	this.userId = userId;
}
 
 
}
