package com.app.sigma.pro.data.constant;

public interface RTConstants {
  final static String APPLICATION_NAME="Sigma Pro Application";
  final static String BASE_PACKAGE="com.app.sigma.pro.web.controller";
  final static String SUCCESS="SUCCESS";
  final static String ERROR="ERROR";
  final static String DATA_NOT_FOUND="DATA NOT FOUND";
  final static String STARTED="STARTED";
  final static String COMPLETED="COMPLETED";
  final static String ALREADY_EXISTS="ALREADY_EXISTS";
}
