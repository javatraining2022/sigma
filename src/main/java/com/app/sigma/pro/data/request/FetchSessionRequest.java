package com.app.sigma.pro.data.request;

public class FetchSessionRequest {
	private long tutorId;
	private long courseId;
	public long getTutorId() {
		return tutorId;
	}
	public void setTutorId(long tutorId) {
		this.tutorId = tutorId;
	}
	public long getCourseId() {
		return courseId;
	}
	public void setCourseId(long courseId) {
		this.courseId = courseId;
	}
	
}
