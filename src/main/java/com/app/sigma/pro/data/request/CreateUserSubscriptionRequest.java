package com.app.sigma.pro.data.request;

public class CreateUserSubscriptionRequest {
	private long subscriptionId;
	private long subscriberId;
	public long getSubscriptionId() {
		return subscriptionId;
	}
	public void setSubscriptionId(long subscriptionId) {
		this.subscriptionId = subscriptionId;
	}
	public long getSubscriberId() {
		return subscriberId;
	}
	public void setSubscriberId(long subscriberId) {
		this.subscriberId = subscriberId;
	}
	
}
