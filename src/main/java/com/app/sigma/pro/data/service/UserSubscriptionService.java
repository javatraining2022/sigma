package com.app.sigma.pro.data.service;

import java.util.List;

import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.app.sigma.pro.data.constant.RTConstants;
import com.app.sigma.pro.data.jpa.persistance.Subscription;
import com.app.sigma.pro.data.jpa.persistance.UserSubscription;
import com.app.sigma.pro.data.repository.SubscriptionRepository;
import com.app.sigma.pro.data.repository.UserSubscriptionRepository;
import com.app.sigma.pro.data.request.CreateSubscriptionRequest;
import com.app.sigma.pro.data.request.CreateUserSubscriptionRequest;
import com.app.sigma.pro.web.response.SubscriptionResponse;
import com.app.sigma.pro.web.response.SubscriptionsResponse;
import com.app.sigma.pro.web.response.UserSubscriptionResponse;
import com.app.sigma.pro.web.response.UserSubscriptionsResponse;

@Service
public class UserSubscriptionService {
	@Autowired
	private UserSubscriptionRepository userSubscriptionRepository;
	@Autowired
	private SubscriptionRepository subscriptionRepository;
	
	private static final Logger LOGGER = LoggerFactory.getLogger(UserSubscriptionService.class);

	@Transactional
	public UserSubscriptionResponse createUserSubscription(final CreateUserSubscriptionRequest req) {
		LOGGER.info("Starting the Session.");
		UserSubscriptionResponse response = new UserSubscriptionResponse();
		UserSubscription us=new UserSubscription();
		us.setSubscriberId(req.getSubscriberId());
		us.setSubscriptionId(req.getSubscriptionId());
		UserSubscription savedUserSubsription =userSubscriptionRepository.save(us);
		if (savedUserSubsription==null) {

			
			response.setUserSubscription(null);
			response.setStatus("ERROR");
			return response;
		}
		response.setUserSubscription(savedUserSubsription);
		response.setStatus("SUCCESS");
		return response;
	}
	@Transactional
	public UserSubscriptionsResponse fetchUserSubscriptions(final long subscriberId) {
		LOGGER.info("Starting the Session.");
		UserSubscriptionsResponse response = new UserSubscriptionsResponse();
		
		List<UserSubscription> us=userSubscriptionRepository.getUserSubscription(subscriberId);
		if(!us.isEmpty()) {
		response.setUserSubscriptions(us);
		response.setStatus("SUCCESS");
		}else{
			response.setUserSubscriptions(null);
			response.setStatus(RTConstants.DATA_NOT_FOUND);	
		}
		return response;
	}

	@Transactional
	public SubscriptionResponse createSubscription(final CreateSubscriptionRequest req) {
		LOGGER.info("Starting the Session.");
		SubscriptionResponse response = new SubscriptionResponse();
		Subscription us=new Subscription();
		us.setColor(req.getSubscriptionThemeColor());
		us.setLimitations(req.getLimitations());
		us.setName(req.getSubscriptionName());
		us.setPrice(req.getSubscriptionPrice());
		us.setValidity(req.getSubscriptionValidity());
		Subscription savedSubsription =subscriptionRepository.save(us);
		if (savedSubsription==null) {
			response.setSubscription(null);
			response.setStatus("ERROR");
			return response;
		}
		response.setSubscription(savedSubsription);
		response.setStatus("SUCCESS");
		return response;
	}
	@Transactional
	public SubscriptionsResponse fetchAllSubscriptions() {
		LOGGER.info("Starting the Session.");
		SubscriptionsResponse response = new SubscriptionsResponse();
		
		List<Subscription> us=subscriptionRepository.findAll();
		if(!us.isEmpty()) {
		response.setSubscriptions(us);
		response.setStatus("SUCCESS");
		}else{
			response.setSubscriptions(null);
			response.setStatus(RTConstants.DATA_NOT_FOUND);	
		}
		return response;
	}
}
