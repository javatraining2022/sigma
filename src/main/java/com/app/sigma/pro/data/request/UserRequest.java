package com.app.sigma.pro.data.request;

import java.io.Serializable;

import javax.validation.constraints.NotBlank;


public class UserRequest implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private long id;
	@NotBlank(message="User Name must not be Null or Empty")
	private String userName;
	@NotBlank(message="Password must not be Null or Empty")
	private String password;
	@NotBlank(message="Email must not be Null or Empty")
	private String email;
	@NotBlank(message="Phone must not be Null or Empty")
	private String phone;
	@NotBlank(message="CountryCode must not be Null or Empty")
	private String countryCode;
	@NotBlank(message="Role must not be Null or Empty")
	private String role;
	@NotBlank(message="Category must not be Null or Empty")
	private String category;
	private String  url;
	private String gender;
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getCountryCode() {
		return countryCode;
	}
	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}
	public String getRole() {
		return role;
	}
	public void setRole(String role) {
		this.role = role;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	
	

	
}
