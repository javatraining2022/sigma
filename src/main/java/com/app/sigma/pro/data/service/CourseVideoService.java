package com.app.sigma.pro.data.service;

import java.util.List;

import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.app.sigma.pro.data.constant.RTConstants;
import com.app.sigma.pro.data.jpa.persistance.CourseVideo;
import com.app.sigma.pro.data.repository.CourseVideoRepository;
import com.app.sigma.pro.data.request.CreateCourseVideoRequest;
import com.app.sigma.pro.web.response.CourseVideoResponse;
import com.app.sigma.pro.web.response.CourseVideosResponse;

@Service
public class CourseVideoService {
	private CourseVideoRepository courseVideoRepository;

	private static final Logger LOGGER = LoggerFactory.getLogger(CourseVideoService.class);

	public CourseVideoService(CourseVideoRepository courseVideoRepository) {
		this.courseVideoRepository = courseVideoRepository;
	}

    @Transactional
	public CourseVideoResponse createVideo(final CreateCourseVideoRequest req) {
		LOGGER.info("Creating the CourseVideo.");
		CourseVideoResponse response = new CourseVideoResponse();
		CourseVideo cv=new CourseVideo();
		cv.setCourseId(req.getCourseId());
		cv.setDescription(req.getDescription());
		cv.setName(req.getCourseName());
		cv.setTutorId(req.getTutorId());
		cv.setUrl(req.getUrl());
		cv.setType(req.getType());
		
		CourseVideo savedVideo =courseVideoRepository.save(cv);
		if (savedVideo==null) {

			
			response.setCourseVideo(null);
			response.setStatus("ERROR");
			return response;
		}
		response.setCourseVideo(savedVideo);
		response.setStatus("SUCCESS");
		return response;
	}
	@Transactional
	public CourseVideosResponse fetchCourseVideos(final long courseId) {
		LOGGER.info("Fetching the Videos.");
		CourseVideosResponse response = new CourseVideosResponse();
		
		List<CourseVideo> cvs=courseVideoRepository.getCourseVideos(courseId);
		if(!cvs.isEmpty()) {
		response.setCourseVideos(cvs);
		response.setStatus("SUCCESS");
		}else{
			response.setCourseVideos(null);
			response.setStatus(RTConstants.DATA_NOT_FOUND);	
		}
		return response;
	}

}
