package com.app.sigma.pro.data.jpa.persistance;

import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "sigma_user")
public class User {

	/** Unique identifier of the User - typically a primary key. */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", nullable = false)
	private Long id;

	/** UserName of the User. */
	@Column(name = "user_name", length = 128)
	private String userName;
	
	/** gender of the User. */
	@Column(name = "gender", length = 10)
	private String gender;


	/** Profile Image Url of the User. */
	@Column(name = "image_url")
	private String imageUrl;

	public String getImageUrl() {
		return imageUrl;
	}

	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	/** Mobile Number of the User. */
	@Column(name = "phone", length = 14)
	private String phone;

	/** Country code of the User. */
	@Column(name = "country_code", length = 6)
	private String countryCode;
	/** Tutor Flag **/
	@Column(name = "tutor")
	private boolean tutor;

	/** UserName of the User. */
	@Column(name = "email", length = 64)
	private String email;

	/** Password of the User. */
	@Column(name = "password", length = 128)
	@JsonIgnore
	private String password;

	@Column(name = "category", length = 128)
	private String category;

	/** Many to Many mappings between the User and Role. */
	@ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinTable(name = "sigma_user_role", joinColumns = { @JoinColumn(name = "sigma_user_id") }, inverseJoinColumns = {
			@JoinColumn(name = "sigma_role_id") })
	private Set<Role> roles = new HashSet<>();

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getCountryCode() {
		return countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	public boolean isTutor() {
		return tutor;
	}

	public void setTutor(boolean tutor) {
		this.tutor = tutor;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Set<Role> getRoles() {
		return roles;
	}

	public void setRoles(Set<Role> roles) {
		this.roles = roles;
	}

	/**
	 * This method returns a collection of role names that are assigned to this
	 * user.
	 *
	 * @return Collection of role names.
	 */
	public Collection<String> getAssignedRoles() {
		return Optional.ofNullable(roles).orElse(Collections.emptySet()).stream().map(Role::getName)
				.collect(Collectors.toSet());
	}

}
