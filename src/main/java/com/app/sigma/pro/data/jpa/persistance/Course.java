package com.app.sigma.pro.data.jpa.persistance;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="course")
public class Course {
	
	/** Unique identifier of the Session - typically a primary key. */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", nullable = false)
	private Long id;
	
	/** Recognation Id  of the Course. */
	@Column(name = "recgId", length = 128)
	private String recgId;
	
	/** Name of the Course. */
	@Column(name = "name", length = 128)
	private String name;
	
	/** user id  of the Course. */
	@Column(name = "user_id")
	 private Long userId;
	
	/** Duration of the Course. */
	@Column(name = "duration_days", length = 128)
    private String durationDays;
	
	/** Category of the Course. */
	@Column(name = "category", length = 128)
	private String category;
	
	/**Description of the course. */
	@Column(name = "description", length = 128)
	private String description;

	/**cost of the course. */
	@Column(name = "cost", length = 128)
	private String cost;
	/** tutor name of the course. */
	@Column(name = "tutor_name")
	private String tutorName;
	
	public String getRecgId() {
		return recgId;
	}

	public void setRecgId(String recgId) {
		this.recgId = recgId;
	}

	public Long getId() {
		return id;
	}

	public String getTutorName() {
		return tutorName;
	}

	public void setTutorName(String tutorName) {
		this.tutorName = tutorName;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getCost() {
		return cost;
	}

	public void setCost(String cost) {
		this.cost = cost;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDurationDays() {
		return durationDays;
	}

	public void setDurationDays(String durationDays) {
		this.durationDays = durationDays;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	
}
