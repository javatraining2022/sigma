package com.app.sigma.pro.data.jpa.persistance;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "subscription")
public class Subscription {
	/** Unique identifier of the Subscription - typically a primary key. */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", nullable = false)
	private Long id;

	/** Name of the Subscription. */
	@Column(name = "name", length = 128)
	private String name;

	/** validity of the Subscription. */
	@Column(name = "validity", length = 128)
	private String validity;

	/** price of the Subscription. */
	@Column(name = "price", length = 128)
	private String price;

	/** limitations of the Subscription. */
	@Column(name = "limitations", length = 128)
	private String limitations;

	/** color of the Subscription. */
	@Column(name = "color", length = 128)
	private String color;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getValidity() {
		return validity;
	}

	public void setValidity(String validity) {
		this.validity = validity;
	}

	public String getPrice() {
		return price;
	}

	public void setPrice(String price) {
		this.price = price;
	}

	public String getLimitations() {
		return limitations;
	}

	public void setLimitations(String limitations) {
		this.limitations = limitations;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}
	

}
