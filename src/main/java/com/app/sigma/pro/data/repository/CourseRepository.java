package com.app.sigma.pro.data.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.app.sigma.pro.data.jpa.persistance.Course;
import com.app.sigma.pro.data.jpa.persistance.Session;
@Repository
public interface CourseRepository extends JpaRepository<Course, Long> {
	
	@Query(value = "SELECT c FROM Course c WHERE c.category = :category")
	List<Course> getCoursesByCategory(@Param("category")String category);
	@Query(value = "SELECT c FROM Course c WHERE c.category = :category AND c.userId = :userId")
	List<Course> getCoursesByCategoryAndUserId(String category,Long userId);
    Course findByRecgId(String recgId);

}
