package com.app.sigma.pro.data.request;

public class CreateSubscriptionRequest {


	private String subscriptionName;

	private String subscriptionValidity;

	private String subscriptionPrice;

	private String limitations;

	private String subscriptionThemeColor;

	public String getSubscriptionName() {
		return subscriptionName;
	}

	public void setSubscriptionName(String subscriptionName) {
		this.subscriptionName = subscriptionName;
	}

	public String getSubscriptionValidity() {
		return subscriptionValidity;
	}

	public void setSubscriptionValidity(String subscriptionValidity) {
		this.subscriptionValidity = subscriptionValidity;
	}

	public String getSubscriptionPrice() {
		return subscriptionPrice;
	}

	public void setSubscriptionPrice(String subscriptionPrice) {
		this.subscriptionPrice = subscriptionPrice;
	}

	public String getLimitations() {
		return limitations;
	}

	public void setLimitations(String limitations) {
		this.limitations = limitations;
	}

	public String getSubscriptionThemeColor() {
		return subscriptionThemeColor;
	}

	public void setSubscriptionThemeColor(String subscriptionThemeColor) {
		this.subscriptionThemeColor = subscriptionThemeColor;
	}
	
	

}
