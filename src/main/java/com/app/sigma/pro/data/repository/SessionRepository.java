package com.app.sigma.pro.data.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.app.sigma.pro.data.jpa.persistance.Session;

@Repository
public interface SessionRepository extends JpaRepository<Session, Long> {
	@Query(value = "SELECT s FROM Session s WHERE s.courseId = :courseId AND s.tutorId = :tutorId")
	List<Session> getSessionsByTutors(@Param("courseId")long courseId,@Param("tutorId")long tutorId);
	@Query(value = "SELECT s FROM Session s WHERE s.courseId = :courseId")
	List<Session> getSessions(@Param("courseId")long courseId);

}
