package com.app.sigma.pro.data.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.app.sigma.pro.data.jpa.persistance.CourseVideo;
import com.app.sigma.pro.data.jpa.persistance.Session;

@Repository
public interface CourseVideoRepository extends JpaRepository<CourseVideo, Long> {
	@Query(value = "SELECT cv FROM CourseVideo cv WHERE cv.courseId = :courseId AND cv.tutorId = :tutorId")
	List<CourseVideo> getCourseVideosByTutors(@Param("courseId")long courseId,@Param("tutorId")long tutorId);
	@Query(value = "SELECT cv FROM CourseVideo cv WHERE cv.courseId = :courseId")
	List<CourseVideo> getCourseVideos(@Param("courseId")long courseId);

}
