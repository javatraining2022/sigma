package com.app.sigma.pro.data.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.app.sigma.pro.data.jpa.persistance.User;


@Repository
public interface UserRepository extends JpaRepository<User,Long> {
	User findByUserName(String username);

}
