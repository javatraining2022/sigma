package com.app.sigma.pro.data.handler;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.app.sigma.pro.data.jpa.model.RTError;
import com.app.sigma.pro.web.response.BaseResponse;

@ControllerAdvice
public class DefaultCustomExceptionHandler extends ResponseEntityExceptionHandler{
	 /** Reference to the logger. */
    private static final Logger LOG = LoggerFactory.getLogger(DefaultCustomExceptionHandler.class);

    @ExceptionHandler(Exception.class)
    public final ResponseEntity<Object> handleAllExceptions(Exception ex, WebRequest request) {
        List<RTError> details = new ArrayList<>();
        RTError error=new RTError();
        error.setName(ex.getLocalizedMessage());
        error.setDescription("Exception Occured due to "+ex.getLocalizedMessage());
        details.add(error);
        BaseResponse response = new BaseResponse();
        response.setErrors(details);
        response.setStatus("ERROR");
        return new ResponseEntity(response, HttpStatus.INTERNAL_SERVER_ERROR);
    }
 
 
    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        List<RTError> errors = new ArrayList<>();
        for(ObjectError error : ex.getBindingResult().getAllErrors()) {
        	 RTError rtError=new RTError();
        	 rtError.setName(error.getDefaultMessage());
        	 rtError.setDescription(error.getCode());
            errors.add(rtError);
        }
        BaseResponse response = new BaseResponse();
        response.setErrors(errors);
        response.setStatus("ERROR");
        return new ResponseEntity(response, HttpStatus.BAD_REQUEST);
    }
}
